package inacap.test.holamundoiei4d.vista;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import inacap.test.holamundoiei4d.MainActivity;
import inacap.test.holamundoiei4d.R;
import inacap.test.holamundoiei4d.controlador.UsuariosController;
import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.modelo.sqlite.UsuariosModel;

/**
 * Created by mitlley on 23-08-17.
 */

public class FormularioActivity extends AppCompatActivity {

    private EditText etUsername, etPassword1, etPassword2;
    private Button btRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        this.etUsername = (EditText) findViewById(R.id.etUsername);
        this.etPassword1 = (EditText) findViewById(R.id.etPassword1);
        this.etPassword2 = (EditText) findViewById(R.id.etPassword2);

        this.btRegister = (Button) findViewById(R.id.btRegister);

        this.btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            // Creamos el usuario a traves de la capa Controlador

                String nombre_usuario = etUsername.getText().toString();
                String password1 = etPassword1.getText().toString();
                String password2 = etPassword2.getText().toString();

                try {
                    UsuariosController controller = new UsuariosController(getApplicationContext());
                    controller.crearUsuario(nombre_usuario, password1, password2);

                    Intent v2 = new Intent(FormularioActivity.this, MainActivity.class);
                    startActivity(v2);

                    finish();
                } catch(Exception e){
                    String mensaje = e.getMessage();
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}



















