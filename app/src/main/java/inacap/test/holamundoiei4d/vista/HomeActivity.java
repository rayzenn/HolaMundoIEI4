package inacap.test.holamundoiei4d.vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import inacap.test.holamundoiei4d.MainActivity;
import inacap.test.holamundoiei4d.R;
import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;

public class HomeActivity extends AppCompatActivity {


    private Button btCerrarSesion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.btCerrarSesion = (Button) findViewById(R.id.btCerrarSesion);

        this.btCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //los datos de inicio de sesion se borraran y empezaran en el main activity

                SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERECENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sesiones.edit();

                editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, false);

                editor.commit();

                Intent v = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(v);

                finish();
            }
        });


    }
}
