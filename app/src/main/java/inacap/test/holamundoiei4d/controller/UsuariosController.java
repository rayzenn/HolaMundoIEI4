package inacap.test.holamundoiei4d.controller;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.modelo.sqlite.UsuariosModel;

import static inacap.test.holamundoiei4d.R.id.etPassword1;
import static inacap.test.holamundoiei4d.R.id.etUsername;

/**
 * Created by 19414383-4 on 01/09/2017.
 */

public class UsuariosController {

        //declara el objeto

        private UsuariosModel usuariosModel;

        public UsuariosController(Context context) {
            this.usuariosModel = new UsuariosModel(context);
        }

        public void crearUsuario(String username, String password1, String password2) throws Exception{

            if(!password1.equals(password2)){
                //Si las contraseñas no coinciden lanzamos un Exception
                //Este debe ser manejado por la Activity
                throw new Exception("Contraseñas no coinciden");
            }



            //Llamar UsuarioModel para agregar un usuario a la base de datos
            ContentValues usuario = new ContentValues();
            usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME, username);
            usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD, password1);

            this.usuariosModel.crearUsuario(usuario);


        }

    }
